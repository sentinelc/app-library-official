# ZEEK

A powerful framework for network traffic analysis and security monitoring.

## About this service

This is ZEEK pre-configured to run directly on a security gateway. It will monitor the traffic passing through the
network appliance on which it is installed.

For example, if you install it on a router, it will monitor all the traffic being routed.

On an Access Point, it will monitor only the traffic bridged through that access point.

## Log files

Log files are generated in a local volume named 'logs'. Each log file is rotated daily.

You can select the standard csv log format or JSON log format using an install time option. JSON
must be selected when enabling the fluent-bit side-container.

## Fluent-bit

An optional fluent-bit side-container can be enabled at installation.

The fluent-bit configuration is meant to use an Elastic search output. The zeek
logs are filtered to match the Elastic Common Schema (ECS).

Multiple configuration items apply to the elastic search output. Refer to the
[fluent-bit es output plugin documentation](https://docs.fluentbit.io/manual/pipeline/outputs/elasticsearch)
for details.

See also the [fluent-bit container source code](https://gitlab.com/sentinelc/containers/fluentbit-zeek) for reference.

