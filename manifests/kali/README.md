# Kali Linux adapted for SentinelC

Kali linux accessible using a web-based terminal.

## Admin password

The initial username is 'admin' and the password will be generated randomly and shown once at the end of the installation process. Copy paste the password before closing the installation confirmation step.

This user has sudo privileges, so you can gain root access by using `sudo -s` for example.

## Access

A web terminal based on the [wetty project](https://github.com/butlerx/wetty) is accessible remotely using the 'Link to web based interface' button in the installed service page.

The SSH port can also be opened to the VLAN where you will be installing the service, by selecting the "Enable ssh service" install option. This option will allow a local client inside the same vlan to connect using the SSH protocol.

You can combine the SSH port using a Port forwarding rule to expose the ssh port remotely, just like you can expose any other port.

## Persisting data

A persistent volume named data is created at the `/data` path inside the service. You can save any important data in this directory. These files are then remotely accessible from the sentinelc admin console under Volumes.

The volume can also be preserved even if you uninstall the service.

## Pre-installed tools

This is a minimal kali install, the following packages are pre-installed:

- whois
- nano
- curl
- wget
- iputils-ping
- python3
- iproute2
- command-not-found
- bind9-dnsutils
- nmap

Install more tools using:

```
apt update
apt install <package>
```
