# TShark

TShark is the command line version of the popular Wireshark packet analyzer and is part of the Wireshark project.

This recipe will run TShark on a dedicated Ethernet port. You will see all packets received printed in the logs.

This is mostly a tech demo of the dedicated Ethernet port feature, but it can be useful to quickly see live packets received on
a port.

The recipe requires an available Ethernet port on the appliance where it is installed. The port will be dedicated to the
service and cannot be used for anything else once the service is installed.

To free the port, simply uninstall the service.
