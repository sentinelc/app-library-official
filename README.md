# SentinelC official app library

The official SentinelC app libary is the only one that is pre-configured in
the cloud controller.

The following quality standards should be met for all apps in the official feed:

- Container images should be fixed tags. No rolling or generic tags such as `:latest` or `:2.0` that can change without notice.
- The source images used should be under the sentinelc team or a reputable organization.
- Recipes should be documented with a README explaining how the app is integrated and how to use it.
- Apps should work reliably. No beta quality integration. Use demo or test feeds for that.
- Minimal storage and ram requirements should be specified for all recipes using the `requirements` fields in the info file.

